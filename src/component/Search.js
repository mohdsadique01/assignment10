import React from 'react'
import "../App.css"

function Search() {
  return (
    <div className='flex search-cont'>
      <div className='search'>
        <div >
          <img className='search-img' src='https://www.synevo.pl/wp-content/uploads/2020/05/synevo-banner-blue.jpg'/>

        </div>
        <div>
          <p style={{color:'orange',marginLeft:20}}>Your Blood Test is ready</p>
        </div>
      </div>
      <div className='searchbox'>
        <input placeholder='search'></input>
        <img className='icon' src='https://www.pngfind.com/pngs/m/617-6173786_small-icon-png-search-icon-svg-transparent-png.png' alt='searchicon'></img>
      </div>
    </div>
  )
}

export default Search
