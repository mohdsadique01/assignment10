import React from 'react'
import "../App.css"

function DiscountChild({specializations,account,percentage}) {
  return (
    <div className='discount-box'>
        <div>
           <p className='discount-color'>{specializations}</p>
        </div>
        <div className='discount-percent'>
            <div>
                <img className='discount-img' src= {account}/>

            </div>
            <div className='discount-number'>
                <p className='discount-color bck'>{percentage}</p>
            </div>
        </div>
      
    </div>
  )
}

export default DiscountChild
