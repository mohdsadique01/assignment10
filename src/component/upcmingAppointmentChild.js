import React from 'react'
import "../App.css"
function upcmingAppointmentChild({img,name, specialization, date,time ,isLargeRow=false }) {
  return (
    <div className='profile-box'>
      <div className='img-cont'>
        <img className='profile-image' src={img}/>
      </div>
      <div className='detail'>
        <p><h3>{name}</h3></p>
        <p style={{color:'gray'}}>{specialization}</p>
        <span className='appointment-time'>
            <p className='flex pc'><img className='icon' src='https://mpng.subpng.com/20190614/ufs/kisspng-calendar-date-computer-icons-clip-art-portable-net-makeup-5d03354e687745.8711722615604913424279.jpg'/>{date}</p> 
          <p className='flex pc'><img className='icon' src='https://as2.ftcdn.net/v2/jpg/02/01/13/15/1000_F_201131573_KDShssc2eWLfUIdHAmZ9ibhRMCBtt583.jpg'/>{time}</p>
          </span>
      </div>
    </div>
  )
}

export default upcmingAppointmentChild
