
import React from 'react'
import"../App.css"
function ServiceChild({img,service}) {
  return (
    <div className='servic-box'>
      <div>
        <img className='icon' src={img} />
      </div>
      <div>
        <h3>{service}</h3>
      </div>
    </div>
  )
}

export default ServiceChild
