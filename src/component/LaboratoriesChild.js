import React from 'react'
import "../App.css"
function LaboratoriesChild({test,price,discount}) {
  return (
    <div className='Laboratories-box' >
      <img className='icon' src='https://cdn-icons-png.flaticon.com/512/78/78524.png'/>
      <h3>Test : {test}</h3>
      <h3> Price : {price}</h3>
      <h3> Discount : {discount}</h3> 
    </div>
  )
}

export default LaboratoriesChild
