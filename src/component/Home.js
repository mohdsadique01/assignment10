import React, { Component } from 'react'
import Service from './service'
import Discount from './Discount'
import Search from './Search'
import Nav from './nav'
import Footer from './footer'
import UpcomingAppointment from './upcomingAppointment' 
export default class Home extends Component {
  render() {
    return (
      <div>
       <Nav />
       <Search />
      <Service />
      <UpcomingAppointment isLargeRow />
      <Discount />
      <Footer />


      </div>
    )
  }
}
