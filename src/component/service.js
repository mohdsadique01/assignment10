import React from 'react'
import "./service.css"
import { Link } from 'react-router-dom'
import ServiceChild from './serviceChild'
function Service() {
  return (
    <div>
      <div>
        <p><h2 style={{marginLeft:20}}>Service</h2></p>
      </div>
      <div className='service'>
        <Link to="/form">
        <ServiceChild 
         img = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLVjfNdkeF-5aiFOyMaPTPUSS47g6CTaWzVQ&usqp=CAU"
         service = "Consultations" 
        />
        </Link>
        <Link to="medicine">
        <ServiceChild 
        img = "https://www.iconpacks.net/icons/2/free-medicine-icon-3198-thumb.png"
        service ="Medicines"
        />
        </Link>
        <Link to="lab">
        <ServiceChild 
         img = "https://cdn-icons-png.flaticon.com/512/78/78524.png"
         service ="Laboratories"
        />
        </Link>

      </div>
    </div>
  )
}

export default Service
