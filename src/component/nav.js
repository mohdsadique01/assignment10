import React from 'react'
import "../App.css"
function Nav() {
  return (
    <div className='nav'>
      <div className='flex' style={{marginLeft:40}}>
       <h2 className='profile-name'>Hey, Jullia</h2>
       <img className='icon' src='https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Twemoji_1f600.svg/1200px-Twemoji_1f600.svg.png'/>
      </div>
      <div className='icon' style={{marginRight:40}}>
            <img className='icon-img' src='https://image.shutterstock.com/image-vector/notification-icon-vector-material-design-260nw-759841507.jpg' />
      </div>
    </div>
  )
}

export default Nav
