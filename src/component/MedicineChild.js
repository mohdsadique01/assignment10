import React from 'react'
import "../App.css"
function MedicineChild({tablet,price,discount}) {
  return (
    <div className='medicine-box'>
     <div>
         <img className='icon' src='https://previews.123rf.com/images/dedegrphc/dedegrphc2002/dedegrphc200202341/140030586-medical-pills-icon-vector-logo-illustration-design-template.jpg' />
     </div>
     <div>
         <h3>Tablet : {tablet}</h3> 
    </div>
     <div>
         <h3> Price : {price}</h3>
    </div>
    <div>
          <h3> Discount : {discount}</h3>   
    </div>
    </div>
  )
}

export default MedicineChild
