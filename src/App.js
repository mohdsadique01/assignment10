import React from 'react'
import "./App.css"
import Home from './component/Home'
import Form from './component/Form'
import Medicine from './component/Medicine'
import Laboratories from './component/Laboratories'
import { BrowserRouter as Router ,Route,Switch } from 'react-router-dom'
function App() {
  return (
    <Router>
    <div className='app'>
        
        
      <Switch>
        <Route exact path='/'>
           <Home/>
        </Route>
      <Route path='/medicine'>
      <Medicine />
      </Route>
      <Route path='/lab'>
      <Laboratories />
      </Route>
      <Route path='/form'>
        <Form/>
      </Route>
    
      </Switch>
    </div>
    </Router>
  )
}

export default App
